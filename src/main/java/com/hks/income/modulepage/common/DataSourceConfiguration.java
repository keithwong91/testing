package com.hks.income.modulepage.common;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {
    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.db.datasource.driver.class.name}")
    private String driverClassName;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.db.datasource.max.pool.size}")
    private int maxPoolSize;

    @Value("${spring.datasource.minIdle}")
    private int minIdle;

    @Bean
    public DataSource dataSource() {
        HikariConfig dataSourceConfig = new HikariConfig();
        dataSourceConfig.setJdbcUrl(url);
        dataSourceConfig.setUsername(username);
        dataSourceConfig.setPassword(password);
        dataSourceConfig.setMaximumPoolSize(maxPoolSize);
        dataSourceConfig.setMinimumIdle(minIdle);
        dataSourceConfig.setDriverClassName(driverClassName);
        HikariDataSource dataSource = new HikariDataSource(dataSourceConfig);
        return dataSource;
    }
}
