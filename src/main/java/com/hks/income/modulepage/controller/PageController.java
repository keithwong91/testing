package com.hks.income.modulepage.controller;

import com.hks.income.modulepage.common.DataSourceConfiguration;
import com.hks.income.modulepage.vm.PageTFieldVM;
import com.hks.income.modulepage.vm.TFieldRowVMMapper;
import com.hks.income.modulepage.vm.TFieldVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class PageController {

    @Autowired
    private JdbcTemplate jdbcTemplate = new JdbcTemplate();

    @Autowired
    DataSourceConfiguration dataSourceConfiguration;

    public ArrayList<PageTFieldVM> getAllPage() {
        List<String> pages = Arrays.asList(new String[]{"index", "profile", "profileInfo", "storyboard", "financialgraph"});
        ArrayList<PageTFieldVM> response = new ArrayList<>();

        this.jdbcTemplate.setDataSource(dataSourceConfiguration.dataSource());
        String sql = "select * from Tfield where active = 1;";
        List<TFieldVM> tFieldresult = jdbcTemplate.query(sql, new TFieldRowVMMapper());

        for (String page : pages) {
            response.add(new PageTFieldVM(page, tFieldresult.stream().filter(tFieldVM -> page.equals(tFieldVM.getPage())).collect(Collectors.toList())));
        }

        return response;
    }

}
