package com.hks.income.modulepage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModulePageApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModulePageApplication.class, args);
	}

}
