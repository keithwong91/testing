package com.hks.income.modulepage.restservice;

import com.hks.income.modulepage.controller.PageController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/api/v1")
public class ApiController {

    @GetMapping("/test")
    public ResponseEntity<?> test() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("success", true);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/getAllPageField")
    public ResponseEntity<?> getAllPageField() {
        PageController pageController = new PageController();
        return new ResponseEntity<>(pageController.getAllPage(), HttpStatus.OK);
    }

}
