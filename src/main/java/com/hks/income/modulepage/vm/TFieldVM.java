package com.hks.income.modulepage.vm;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "Tfield")
@Getter
@Setter
public class TFieldVM {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FieldName")
    public String fieldName;

    @Column(name = "InputType")
    public String inputType;

    @Column(name = "Type")
    public String type;

    @Column(name = "OptionValue")
    public String optionValue;

    @Column(name = "ShowLabel")
    public int showLabel;

    @Column(name = "PlaceHolder")
    public String placeHolder;

    @Column(name = "Page")
    public String page;

    @Column(name = "Sequence")
    public int sequence;

    @Column(name = "Label")
    public String label;

    @Column(name = "ReadOnly")
    public int readOnly;

    @Column(name = "Active")
    public int active;

    @Column(name = "CreateAt")
    public Date createAt;

    @Column(name = "UpdateAt")
    public Date updateAt;
}
