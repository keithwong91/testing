package com.hks.income.modulepage.vm;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TFieldRowVMMapper implements RowMapper<TFieldVM> {

    @Override
    public TFieldVM mapRow(ResultSet rs, int rowNum) throws SQLException {

        TFieldVM tFieldVM = new TFieldVM();
        tFieldVM.setId(rs.getLong("ID"));
        tFieldVM.setFieldName(rs.getString("FieldName"));
        tFieldVM.setLabel(rs.getString("Label"));
        tFieldVM.setType(rs.getString("Type"));
        tFieldVM.setPage(rs.getString("Page"));
        tFieldVM.setSequence(rs.getInt("Sequence"));
        tFieldVM.setLabel(rs.getString("Label"));
        tFieldVM.setPlaceHolder(rs.getString("PlaceHolder"));
        tFieldVM.setOptionValue(rs.getString("OptionValue"));
        tFieldVM.setReadOnly(rs.getInt("ReadOnly"));
        tFieldVM.setShowLabel(rs.getInt("ShowLabel"));
        tFieldVM.setInputType(rs.getString("InputType"));
        tFieldVM.setActive(rs.getInt("Active"));
        return tFieldVM;

    }
}
