package com.hks.income.modulepage.vm;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageTFieldVM {

    public PageTFieldVM(String page, List<TFieldVM> fieldList) {
        this.page = page;
        this.fieldList = fieldList;
    }

    public String page;
    public List<TFieldVM> fieldList;
}
